lens_calibrate.py
=================

Tutorial
--------

You can find a complete tutorial about lens calibration and the use of the
lens_calibrate.py script here:

https://pixls.us/articles/create-lens-calibration-data-for-lensfun/

Requirements
------------

The script needs the following dependencies to be installed on your system:

* python3
* python3-pip
* darktable-cli ([darktable](https://darktable.org) >= 3.0.0)
* tca_correct ([hugin](http://hugin.sourceforge.net) >= 2018)
* convert ([ImageMagick](https://www.imagemagick.org/script/index.php) or [GraphicsMagick](http://www.graphicsmagick.org))
* gnuplot

The python3 libs are better installed using pip3.
* py3exiv2 ([py3exiv2](http://py3exiv2.tuxfamily.org/) >= 0.2.1)
* numpy
* scipy
* PyPDF2

The problem with importing >exiv2.metatada module> 
--------
On original project ([lens_calibrate](https://gitlab.com/cryptomilk/lens_calibrate) from Andreas Schneider), when importing ImageMetadata from exvi2.metadata retuns error.
After following the venv setup without success after hours, I decided to re-write some lines to make something work on my way.

Running the calibration
-----------------------

To setup the required directory structure simply run:

    ./lens_calibrate.py init

The next step is to copy the RAW files you created to the corresponding
directories.

Once you have done that run:

    ./lens_calibrate.py distortion

This will create tiff file you can use to figure out the the lens distortion
values (a), (b) and (c) using hugin. It will also create a lenses.conf where
you need to fill in missing values. **Run this command with one file inside the 
folder to create the conf file, even if you don't want to do distortion correction.**
Can be any file from same lens just to fill the conf file with correct information.


If you want TCA corrections run:

    ./lens_calibrate.py tca

If you want vignetting corrections run:

    ./lens_calibrate.py vignetting

Once you have created data for all corrections you can generate an xml file
which can be consumed by lensfun. Just call:

    ./lens_calibrate.py generate_xml

Using and testing your calibration
----------------------------------

To use the data in your favourite software you just have to copy the generated
lensfun.xml file to:

    ~/.local/share/lensfun/


Send your data to lensfun
-------------------------

If you wonder if your lens is supported check the lens database at:

https://wilson.bronger.org/lensfun_coverage.html

The database is updated daily.

To add lens data to the lensfun project first run:

    ./lens_calibrate.py ship

This will create a file `lensfun_calibration.tar.xz` including the xml file and
the plots showing if the calibration data is valid.

With that tarball please open a bug at

* https://github.com/lensfun/lensfun/issues/

and provide the `lensfun_calibration.tar.xz` file.
